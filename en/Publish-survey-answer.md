---
title: Publish submitted answers
layout: default
nav_order: 3
parent: Surveys
---


# Publish submitted answers
{: .fs-9 }

By default, all answers submitted through your Survey are either pending or private, depending on you Map's settings. Here is how to publish the suggested sites.
{: .fs-5 .fw-500 }
---



## Check the email notification

For every answer submitted through the Survey, you will get an email notification with the name of your map and a link to the survey answer. Click the link in the email and login if necessary, in order to review the submission.

## Review and publish the site

You can see that the current status of the survey answer is pending. At this point you can review and update any data, then, at the bottom of the page choose whether to approve or reject this survey answer.

  <img src="/img/answer_approve_reject_options.webp" alt="review-answer" width="500" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

When approving it, a site corresponding to this answer automatically gets published on the map. If it is rejected, it will be available in the list of answers for your survey for future reference, but no site will be created on the map.

That’s all you need to do for this suggested site to appear on your Green Map!

Here are more details about the data you can update and refine for each survey answer. For example, you can:

- Edit the Title or Description to correct or enhance it. Then click on Save.

  <img src="/img/edit_site_description.webp" alt="edit-site" width="500" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

- Tip: adding a website link to the description can be done two ways. You can copy-paste the **web address** directly or highlight the words you want to link and use the toolbar to add a link. Click the link symbol on the right and paste the **web address** below, then click your keyboard’s return (or enter) key. You’ll see the linked words. Click Save.

  <img src="/img/set_link_1.webp" alt="edit-site" width="300" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

  <img src="/img/set_link_2.webp" alt="edit-site" width="300" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

  <img src="/img/set_link_3.webp" alt="edit-site" width="300" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

- Change the Primary Icon by clicking the one that you think is most appropriate to appear on the map. You can add more icons, or remove some, too.

  <img src="/img/edit_icons_1.webp" alt="edit-site" width="300" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

  <img src="/img/edit_icons_2.webp" alt="edit-site" width="300" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

- Adjust the location by dragging the map to move the map center to the new location. Then press the Save button.

  <img src="/img/edit_geometry.webp" alt="edit-site" width="500" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>
