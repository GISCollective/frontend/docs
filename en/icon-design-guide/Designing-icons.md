---
title: Designing Icons
layout: default
nav_order: 2
parent: Icon Design Guide
---

# Designing Icons
{: .fs-9 }

To look good on the platform, icons must match the following requirements:
- square image
- transparent background
- SVG image format

You can also upload PNG or JPG images, but they might result in blurry icons. Need an SVG converter? You can search online for free converters, like https://www.freeconvert.com/png-to-svg

A single icon can be the whole set.

Styles lets you change the shape, outline, transparency and other attributes of the icon and sets you create.

You’ll add definitions, introduction to the set, links, etc.

## Getting Started

You can design your own or use familiar symbols (such as your city’s Bus Stop or Recycling Station symbol) that are recognized locally. Make sure the symbols are copyright free or that you get permission to use the symbols.

Start by making a list of icons you need. Try not to make each icon too specific because too many symbols can be overwhelming. Try to combine and broaden the symbol’s meaning so it can be used in multiple places (such as we have done with farmers / local market).

Get help prioritising and reducing the list and you’ll simplify the workload too. While it might take extra time, this can be a great team building project to work on new icons together:
- (co)develop your short list of needed symbols,
- sketch out ideas,
- discuss and debate

## Review the existing icons and use them if there’s a match

If you want to tweak the title or definition, you can use Styles to adapt existing icons. Here’s how: start a new set, then add the ‘parent icons’ you want to change and edit the Style.

Both the original and the changed icon will appear in the legend of your map, but your map will show only the icons you chart.

## Create an image, title, definition for each icon you add or adapt

To create your set, prepare:
- a title
- cover image
- a few words about the set.

To make it easy to upload quickly, we recommend preparing all of these in advance.

You can leave your set in Private mode and get your team’s feedback before making them public.

You can add more icons over time, and edit anytime.

## Icon Design Recommendations

Your icon designs should be iconic - simple, bold, elegant, and contain the essential facts or the essence of the object. Avoid letters, words or numbers, and use clean, clear lines and shapes.

One colour and white only. No half-tones or gradients recommended.

Avoid putting the symbol in a box, however, a white symbol on a solid shape can be effective.

Test your sketch on a few different people, including some of the people you expect to use the map. You can do this in person at an event that introduces your map’s idea to people who might want to join your team or participate in a Survey that adds Markers to the map. It’s a great team building project:
- You can print and post the icon options on a wall and provide a way to add comments.
- Or give people an easy way to respond to images, their titles and definitions by email

View your designs at both very small and large sizes to make sure each icon is legible and memorable.

Consider if they will be used off the map on posters, t-shirts, etc.

Use our Patterns if you want your symbols to harmonise with the Green Map Icons.

Use the fewest elements to effectively communicate the concept.

Get inspiration at the [Noun Project](https://thenounproject.com/handbook/create/#collections) or [Fontawesome](https://fontawesome.com/). And yes, you can have our team consult with you on this important aspect!

## Finalising your design

People may have very different ideas about a symbol’s meaning, and it’s good to have ‘fresh eyes’ review them in progress.

Consider having a graphic designer finalise your sketch to optimise its impact and formatting. (Be sure to tell them if you plan to use the icon in print and ask for SVGs and other formats that you need for the project (perhaps PDF and JPG).

Remember, to look good on the platform, icons must match the following requirements:
- square image
- transparent background
- SVG image format

You can also upload PNG or JPG images, but they might result in blurry icons
