---
title: Adding Icons
layout: default
nav_order: 3
parent: Icon Design Guide
---

# Adding Icons
{: .fs-9 }

## Where does the icon appear on the platform?

- Primary icon - the first icon you select will appear on the base map as the Marker, click it to see the place’s details
- Once clicked, or searched: All secondary icons will appear on the Marker’s details on the left side of the map
- On the Map’s Cover page. Here, each marker has a preview box with a row of icons under the site’s title and photo (yes, these Marker Covers can be made into posters to reach off-line audiences),
- Lastly, on Browse>icons>your icon set(s)

The icon will appear the same on the marker, regardless if it is used on a point (site), or to chart routes (lines) or areas (polygons).

You can make your icons larger, change the background colour, add a border etc with Styles, and add meaning with Attributes (see below).

Your icon set can belong to your team’s maps or be shared. You should own the copyright to icons that you upload unless they are open source and/or you can clearly post permission to use them. You can include full credits and terms of use information in the Icon Set description.

Shared sets are usable by anyone, while non-shared sets can only be used for your own team's maps.
If you want your icon set to become one of the "shared" sets that anyone can use, please contact Green Map System (info@greenmap.org). Your set must be complete with credits, terms of use, titles, definitions, etc. to become shared as a default set.

Note: Green Map System or GISCollective are not responsible for the use of any icons you place on this platform. We reserve the right to make icon sets private if no one is using them or if the set appears unfinished.

## The Image is the starting point

Each Icon needs an image, a short clear title, and a brief definition.

Similar to tagging, there is an ‘other names’ field where you can include related terminology since many people explore the Platform using the Search function.

If you are making several symbols, consider setting categories that separate themes and organise the icons in folders on the platform. Fewer categories (2 or 3) will make your map’s legend easier to use for both your team and for the map’s users. Use broad terms, such as Land, Water, Air.

When naming your set, we suggest including your project’s place, theme or event, or your organisation’s name such as Glasgow’s COP26 Icons or Recovery Icons.

Create a spreadsheet to make it easy for people to comment before you finalise and upload. Yes, you can use your choice of languages!

Image  | Title | Definition | Other Names  | Category  | Comments

We suggest you make a list or spreadsheet to confirm the order of the icons, and to organise your Categories,Titles & Definitions before uploading.

Your Icons will appear on the icon page and in the legend in upload order (the first uploaded is on the top left). You can share this as a user guide later, or keep it as a team resource.

Here’s a spreadsheet we made for the [Food Icons](https://docs.google.com/spreadsheets/d/1jwAl8a8p0qEvnxatkDqjCABIIbQaGYhXiQMdQ_BEAWw/edit?usp=sharing), yours can similarly allow public comment.

<img src="/img/icon-design-guide/spreadsheet-example.webp" alt="spreadsheet-example" width="800"/>

## Add a new Icon set

Login on the platform and a **+** will appear on the menu. Choose **Icon Set** from the drop menu.

Add your set’s title and description (which can include a link), then choose the team that will be using these on their map. Save.

Once the page refreshes, you can add a cover image (square) that signifies your set on the Browse.

On the top right, click the << to make the set public, if you want to make the icon set visible to others outside your team. Leave it private for use only on your map (and while your work is in progress). It’s only visible to your team when it’s private.

## Ready to add icons

Click **+** and select **Icon**.

Add the icon’s title, description, and select your set’s name from the drop menu.

<img src="/img/icon-design-guide/Add-new-Icon.webp" alt="add-new-icon" width="800"/>

Want to reuse a Green Map Icon, and retitle it or change its style or attributes? Use the search at **Parent Icon** and select it (more on Attributes is below).

<img src="/img/icon-design-guide/add-parent-icon.webp" alt="add-parent-icon" width="800"/>

Upload each icon’s image. To look good on the platform, your icons must match the following requirements:
- square image
- transparent background
- SVG image format

You can also upload PNG or JPG images, but they might result in blurry icons.
Click Add, and you will be redirected to the Icon **Edit** page.

<img src="/img/icon-design-guide/edit-icon.webp" alt="edit-icon" width="800"/>

We suggest you test on a map as you go to make sure your icons look as planned. (Remember to click Map Edit and mid-page, at Icons, select your new set of Icons to enable them on that map).

Continue to click **+** and select **Icon** until all your icons are added.

Want to make changes? You can edit any element of your set except the icon order. You can add icons, with the last one you uploaded appearing last on the page.
