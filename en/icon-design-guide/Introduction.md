---
title: Introduction
layout: default
nav_order: 1
parent: Icon Design Guide
---


# Introduction
{: .fs-9 }


## Why create your own icons

Want to highlight unique features in your community that are not covered by the symbol sets included on this platform? Yes, you can create your own set of icons, and mix them with others on your maps! Check **Browse>Icons** to see some examples.

While the globally recognized Green Map Icons set includes 12 categories of globally-recognized symbols for sustainability-related places, we have always assumed locally designed icons can be added to help set the context or add familiar symbols or species on print maps. Adding icon sets is an exciting option with this platform.

<img src="/img/icon-design-guide/Green-map-icons-poster.webp" alt="icons-poster" width="800"/>

As detailed in this in-depth Icon Guide, you can create your local icons:
as part of a team-building process,
work with a designer or
use unique, locally familiar symbols (if you have permission to do so)

Recognizing the importance of local icons, we created an underlying ["Pattern Language"](/img/icon-design-guide/Icon_Pattern_Chart.pdf) so yours can harmonise with our set. It’s fun and this guide will help make it easier! You’ll decide if you want to share them or keep your unique icons just for your Green Map.


Globally recognized Green Map Icons for nature, culture and green living resources are at the heart of our mission (see [bit.ly/GMiconposter](https://bit.ly/GMiconposter)). To make a proper Green Map, approximately 50% of the icons used should be from this set. Appearing on the platform in green, blue and orange for quick recognition of nature, sustainability and cultural resources; other formats of Green Map icon images and posters are available [here](https://www.greenmap.org/stories/green-map-icons-sets/198) for your outreach, promotion, print maps, etc.

## What kind of Markers can I put on the Green Map?

Green Map’s Platform is versatile! In addition to points (sites), you can chart routes (lines) and areas (polygons) - see the [demonstration map](https://bit.ly/OGM2lines), and the related video at the bottom of the **About** page, too. You can upload GPX files or draw your routes on the map (these can be exported to use in Strava or other ‘map my ride’ type apps).

## What icon sets are already available on the Platform?

The Platform’s **Browse** page includes all the icon sets you can add, including
- 170 Green Map Icons, our original lively, informative set, co-designed and evolving since 1995 and recognized in 65 countries - green, blue and orange
- 17 UN 2030 Global Goals icons are at United Nations>[SDGs](https://new.opengreenmap.org/browse/icons/5d49a92ef716120100c5d85e). Add them to show how the featured site is aligned with these important, intersecting goals.
- [Recovery Icons](https://new.opengreenmap.org/browse/icons/5ebc501bf885d601004b69ec) were made during the 2020 Pandemic to highlight public health, recuperation and regeneration to support community recovery. Designed for maps, signs, art, etc, this set is black on the Platform.
- Draft sets - for example, [Local Food](https://new.opengreenmap.org/browse/icons/6067a454a4eddf0100334b54) icons. You can use, comment and help improve these.
- Some projects have added their own custom icon set, like [Chance Brothers Lighthouses](https://new.opengreenmap.org/browse/icons/6241bb18d63fa5010056590e).

More on selecting icons and adding features to the map is in [this tutorial](https://bit.ly/ogm2info) or check the [Add data to your map guide](../../Add-map-data).

## You can adapt existing icons (advanced)

- Styles allow you to change the appearance (marker colour, size, transparency) of an icon
the title or description
- to ‘add geometry’ that turns the marker from a point to an area or route. Lines and polygons are edited the same way as points, with geometry added in the mapping area box (see [demonstration map](https://new.opengreenmap.org/browse/maps/5f4839c1877f040100aaf7db/map-view)).
- Setting attributes on an icon supports public participation with a crowd-sourcing Survey.

## How to add existing icon sets to your map

When you are on your map’s Edit page, scroll down to Icon Sets to enable your selection(s).

<img src="/img/icon-design-guide/select-icon-sets.webp" alt="select-icon-sets" width="800"/>

More than one set of icons can be combined on one map, so for example, you can include Green Map Icons and Recovery icons. Both sets will appear on the legend, which can be tapped by the map’s users to filter the map’s sites, routes and areas (often simply called markers in this Guide and other Platform resources).
