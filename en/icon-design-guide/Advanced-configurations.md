---
title: Advanced Configurations
layout: default
nav_order: 4
parent: Icon Design Guide
---

# Advanced Configurations
{: .fs-9 }

## Styles

Styles adjust the icon’s size, colour, outline, transparency, etc.

If you desire, you can edit your entire set on the **Edit Set** page by clicking **Style** and all of your icons will change appearance. Save and test. It’s easy to try a few settings to get the icons to look as you wish.

You can always reset to the default settings (this is the same as used on Green Map Icons).

Alternatively, Styles can be changed on the individual icon. Then you can add geometry to an icon to chart routes (lines) and areas (polygons filled with a tint of colour).

<img src="/img/icon-design-guide/styles-test.webp" alt="edit-icon" width="800"/>

Check this [demonstration map](https://new.opengreenmap.org/browse/maps/5f4839c1877f040100aaf7db/map-view) which includes the Green Map Icons ready for you to use with lines and areas. You can see how the different tints of areas look, and note the thickness of lines and colour choices as you are making your own selection. We carefully checked each colour against the base map to make sure it stood out (but not too much).

<img src="/img/icon-design-guide/lines-and-areas-chart.webp" alt="edit-icon" width="800"/>

Be patient as it takes a bit of back and forth while you test how the new icon and its line or area appears on the map each time you save the style. Screenshots are helpful as you test different options.

## How to set up Lines and Areas

Start with Styles, click ‘has custom style’ for the options to appear.

<img src="/img/icon-design-guide/styles-options.webp" alt="edit-icon" width="800"/>

If the default basemap appears too busy and colourful where you are mapping, you might want to change the base map on your Map Edit form - select it before you test your colour and line thicknesses, etc.

Scroll down to Points, Lines and Areas, and edit the settings to get the resulting style as you like it. It’s a bit tricky! We made screenshots of settings we liked as we tested. We edited each icon’s definition to note whether it could be used as a line or area.

Each time you create a new icon, it will be easier! Enjoy the process and the outcome!

You can share the story of your map and its icons on GreenMap.org - create a profile and add a project story; write a blog; or connect with us on social media with @GreenMap and we will help spread the word! Questions about icons? Contact us at info@greenmap.org
