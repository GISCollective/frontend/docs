---
title: Icon Design Guide
layout: default
nav_order: 8
has_children: true
---


# Icon Design Guide
{: .fs-9 }

<img src="/img/icon-design-guide/intro.webp" alt="design-guide-intro" width="500"/>

Icons make it easy to understand complexities. They are versatile, helping you flexibly determine what gets collected, and how the data looks on the map.

What do you want to map? Your choice of icons will determine so much about your map making process and  what people see front and centre on the map. If you are seeking site suggestions from the public, icons can be used as survey markers in a Campaign. Icons can be used as an inventory tool, and help you work comprehensively. Locally, icons have been created to complement and extend the Green Map Icons, as seen in our engagement guide, [Mapping our Common Ground](/img/icon-design-guide/Mapping_Our_Common_Ground_2018.pdf).

In this section you can find out more about how to customise the included sets of icons, and learn how to design and add your own set of icons to the Green Map Platform.
