---
title: Articles
layout: default
nav_order: 1
parent: Managing your data
---

# Managing articles

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>


## Adding an article

If you create a website using our platform, you may want to create articles to embed in your website pages.
Here are the steps to add an article:

1. Navigate to your team dashboard.
2. Access the "View articles" option located at the top right corner. ![the dashboard header](../../../img/manage/dashboard/header.webp "the dashboard header")
3. Now click on the "+ Article" button, positioned in the top right.
![the add article button](../../../img/manage/articles/article-header-1.webp "the add article button")
4. You can now add the content. Any article needs to have a title and some text.
5. You can also use the + block inserter icon to incorporate additional elements such as images.
![the new article page](../../../img/manage/articles/new-article.webp "the new article page")
6. If you want you can specify relevant categories using the "+ Add" button under the category section. Using these categories you will be able to filter articles by category on your website pages later on.
7. If you are part of multiple teams, ensure the correct team is selected under the "Team" section for precise attribution.
8. Press the "Add" button to save the article and to be redirected to the edit page.

If there already are articles that have categories, you can select an existing category and add an article that automatically has that category. To do this press the + button for the selected category. In the example below the "About" category was selected, and to create a new article with that category, the "+ About" button can be pressed.

![the add article to category button](../../../img/manage/articles/article-header-2.webp "the add article to category button")


## Editing an article


### Get on the Edit page

After creating an article, you are redirected to the edit page of that article.

You can also get to the edit page of an existing article by:

  - Navigating to the articles list from your dashboard by pressing the "View articles" button located at the top right corner, and then selecting the edit option on an article card that you want to edit. The option is available from the three dots button. ![the dashboard header](../../../img/manage/dashboard/header.webp "the dashboard header")
  - Selecting the edit option on an article title from the website.

These options are available only if you have rights to edit the article.


### Fields on the Edit page

The `Edit` page has two sections:
  - the main edit section, where you edit the content of your article:
  ![the article content](../../../img/manage/articles/article-content.webp "the article content")

  - and the sidebar which allows you to edit some metadata of the article:

  <img src="/img/manage/articles/article-sidebar.webp" alt="the article sidebar" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

To edit a field from one section you need to press the edit button. In the main section it is represented by a pencil button <img src="/img/manage/pencil.webp" alt="the pencil button" style= "height:1.5em; box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>, and on the sidebar by an edit text <img src="/img/manage/edit.webp" alt="the edit button" style= "height:1.5em; box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>. The only exception is for the cover (which is saved immediately after you selected a picture) and the main article (which is always editable and saved when the save button is pressed).

To help you to better understand what each field does, here is a list with their purpose:

  - **The main article** is the article that will be shown on the website
  - **Categories** are used to group articles. This is useful when you want to allow only a subset of your articles to be shown on a page.
  - **Release date** is used when an article needs to be published on a certain date. We recommend using this, because it helps you and your users to understand how old is a page.
  - **Cover** is the main picture that based on your website design, and will be shown on a card list on an article page.
  - **Photos** represents is a list of pictures that can be shown on your article page.
  - **Team** sets the team that owns the article.
  - **is public** determines if the article can be seen by unauthenticated people or ones that are not part of the team.
  - **original author** shows who created the article. The user shown on this field can edit the article if they are a member or a leader.
  - **slug** is a unique id that is used instead of the id to reference the article.
  - **order** is a numeric value that, based on how your website is configured, will determine its order in a list.

## Deleting an article

Articles can be deleted only if you have edit rights to them. This means that you created them, or the articles belong to a team where you are
an `owner`:

1. Navigate to your team dashboard.
2. Access the "View articles" option located at the top right corner. ![the dashboard header](../../../img/manage/dashboard/header.webp "the dashboard header")
3. Press the three dots button on an article card to open the context menu:
 <img src="/img/manage/articles/article-context-menu.webp" alt="the article context menu" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>
4. Press the `delete` button
5. You will be prompted to confirm the action:
![the delete article confirmation](../../../img/manage/articles/article-delete-confirmation.webp "the delete article confirmation")
6. If you press `Yes`, the article will be deleted. Note that you won't be able to recover it later.
