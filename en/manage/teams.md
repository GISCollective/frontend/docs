---
title: Teams
layout: default
nav_order: 1
parent: Managing your data
---

# Managing teams

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## Editing the team

## Adding a new user to your team

1. Make sure the user exists on our platform. If not, follow the steps from the [Create a new account](../Create-account.md) tutorial.
2. Navigate to your team dashboard.
3. Access the "Manage team" option at the top right corner. ![the dashboard header](../../../img/manage/dashboard/header.webp "the dashboard header")
4. Add the user's email in the member's section and select the role. After you type a few letters, emails matching your letters will be suggested. ![add new member](../../../img/manage/team/add-member.webp "the dashboard header")
5. Press the "Add to team" button


## Understanding the access roles

On our platform, you can add users who have guest, member, leader, and owner roles. Each role has a different access to the team data. Here is a summary for each role:

Guest - You can only view your tool's private data. They can only add something for your team.
Member - can create, manage and publish their records, but can't manage some special kinds like maps.
Leaders - can create, manage and publish any records owned by the team, but can't add some particular kinds like maps.
Owner - can do anything with the team data.

If you want more details about what each role can do, you can check [our access rights documentation](/en/Access-rights)


## How to enable members to edit their data

Occasionally, you may need to grant a later-joining team member access to records they didn't author or allow them to edit records they don't own. In this case, you need to edit the record and change the "Original author":

1. Go to the edit page of the feature, event or article
2. On the right panel, press "edit" on the original author section: ![original author section](../../../img/manage/original-author.webp "the original author section")
3. Click on the select field and search and select the user that you want to own the record.
4. Click save
