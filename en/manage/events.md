---
title: Events
layout: default
nav_order: 12
parent: Managing your data
---

# Managing events
Available with a subscription
{: .label .label-blue }

If you have a access to the calendar feature, you can also add events. If you want this feature to be enabled for your team, please contact us.

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>


## Adding an event

To create a new event, follow these steps:

1. Navigate to your team dashboard.
2. Select a calendar to add the event to by clicking on a calendar card.
3. Then click on the "+ Event" button positioned in the top right corner.
![the add event button](../../../img/manage/events/events-header-1.webp "the add event button")
4. Fill in the **name** and the description in the **about** field.
6. If you want to change the calendar the event is added to, you can select it from the **calendar** field.
7. If the calendar has icons sets associated to it, you can select one or more icons for the event. Make the selection by pressing the select icons button:

<img src="/img/manage/events/select-icons.webp" alt="the select icons button" width="100" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

8. Add one or more dates for your event by pressing the __+ event__ button:

<img src="/img/manage/events/plus-event.webp" alt="the add event button" width="100" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

9. Optionally, select a cover image by pressing on the gray cover placeholder:

<img src="/img/manage/events/plus-cover.webp" alt="the add cover button" width="100" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

10. Set a location for your event. If the calendar has a map associated to it, you will receive suggestions with locations on your map. Alternatively, you can just type a location, e.g. an address. Note that in the latter case the event won't be associated to a map.
11. Press the "Add" button to save the event and to get redirected to the edit page.

## Editing an event

After creating an event, you are redirected to the edit page of that event. You can also edit an event by:

  - Navigating to the event list from your dashboard by pressing a calendar card, and then selecting the edit option on an event card that you want to edit.
  - Selecting the edit option on an event title from the website.

These options are available only if you have rights to edit the event.

The `Edit` page has two sections:
  - the main edit section, where you edit the content of your event:
  ![the event content](../../../img/manage/events/event-content.webp "the event content")

  - the sidebar which allows you to edit the metadata of the event

To edit a field from one section you need to press the edit button. In the main section it is represented by a pencil button <img src="/img/manage/pencil.webp" alt="the pencil button" style= "height:1.5em; box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>, and on the sidebar by an edit text <img src="/img/manage/edit.webp" alt="the edit button" style= "height:1.5em; box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>.

To help you to better understand what each field does, here is a list with explanations:

  - **The main article** is the article that will be shown on the website.
  - **cover** is the main picture that based on your website design, will be shown on a card list on an event page.
  - **icons** are the icons associated to the event. This is only available if the calendar has icon sets.
  - **entries** allow you to select dates and times for your event.
  - **location** is a text or a map location(if the calendar has a map) for the event.
  - **is public** determines if the event can be seen by unauthenticated users or ones that are not part of the team.
  - **original author** shows who created the event. The user shown on this field can edit the event if they are a member or a leader.

## Deleting an event

An event can be deleted only if you have edit rights to it. This means that you created it, or the event belongs to a team in which you are an `owner`. To delete an event:

1. Navigate to your calendar dashboard.
2. Click on a event card.
3. Press the three dots button on an event card to open the context menu:
![the event context menu](../../../img/manage/events/event-context-menu.webp "the event context menu")
4. Press the `delete` button.
5. You will be prompted to confirm the action:
![the delete event confirmation](../../../img/manage/events/event-delete-confirmation.webp "the delete event confirmation")
6. If you press `Yes`, the event will be deleted. Note that you won't be able to recover it later.
