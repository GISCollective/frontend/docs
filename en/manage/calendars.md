---
title: Calendars
layout: default
nav_order: 11
parent: Managing your data
---

# Managing calendars
Available with a subscription
{: .label .label-blue }

With access to the calendar feature, you can organize and manage events, schedule project phases, or spotlight noteworthy activities on your map.
If you want this feature to be enabled for your team, please contact us.

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>


## Adding a calendar

To create a new calendar, follow these steps:

1. Navigate to your team dashboard.
2. Then click on the "+" button from the dashboard menu:<br/><img src="../../../img/manage/calendar/plus-button.webp" alt="the add calendar button" width="400px" style="box-shadow: 0px 0px 5px #ccc;
    -moz-box-shadow: 0px 0px 5px #ccc;
    -webkit-box-shadow: 0px 0px 5px #ccc;
    -khtml-box-shadow: 0px 0px 5px #ccc;"/>
3. Fill in the calendar **name** and **description**
4. Set the calendar type. This field determines how the calendar will be used by your team:
    - The **events** option allows you to schedule either one-time or recurring events, similar to the calendar systems you're already familiar with.
    - The **schedules** option lets you set up timetables for shops, markets, or recurring volunteer opportunities.
5. __Optionally__, if you have access to multiple teams that can create calendars, choose the team that will own the calendar.
6. Press **Add** and you will be redirected to the edit page of the newly created calendar.

## Editing a calendar

After creating a calendar, you are redirected to the edit page of that calendar. You can also edit a calendar by:

1. Navigating to your team dashboard
2. Find the calendar record that you want to edit, and press the 3 dots button to open the options:<br/><img src="../../../img/manage/calendar/card-options.webp" alt="the edit calendar button" width="400px" style="box-shadow: 0px 0px 5px #ccc;
    -moz-box-shadow: 0px 0px 5px #ccc;
    -webkit-box-shadow: 0px 0px 5px #ccc;
    -khtml-box-shadow: 0px 0px 5px #ccc;"/>
3. Select the **Edit** options

-- or --

1. Navigating to your team dashboard
2. Find the calendar record that you want to edit, and click the card title to open the calendar dashboard
3. Click on the **Edit** option from the calendar dashboard menu:<br/><img src="../../../img/manage/calendar/edit.webp" alt="the edit calendar button" width="100px" style="box-shadow: 0px 0px 5px #ccc;
    -moz-box-shadow: 0px 0px 5px #ccc;
    -webkit-box-shadow: 0px 0px 5px #ccc;
    -khtml-box-shadow: 0px 0px 5px #ccc;"/>


These options are available only if you have rights to edit the event.

The `Edit` page has two sections:
  - the main edit section, where you edit the content of your event:
  ![the event content](../../../img/manage/events/event-content.webp "the event content")

  - the sidebar which allows you to edit the metadata of the event

To edit a field from one section you need to press the edit button. In the main section it is represented by a pencil button <img src="/img/manage/pencil.webp" alt="the pencil button" style= "height:1.5em; box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>, and on the sidebar by an edit text <img src="/img/manage/edit.webp" alt="the edit button" style= "height:1.5em; box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>.

To help you to better understand what each field does, here is a list with explanations:

  - **The main article** is the article that will be shown on the website.
  - **cover** is the main picture that based on your website design, will be shown on a card list on an event page.
  - **map** specifies which map the calendar events will use to choose locations.
  - **icon sets** specifies which icons can be selected for your events.
  - **attributes** allows you to define extra structured data for your events.
  - **record name** by default, event descriptions will use the word "event". For a more personalized experience for your community, you can customize this text with a term like "volunteering opportunity."

## Deleting a calendar

A calendar can be deleted only if you have edit rights to it. This means that you created it, or the calendar belongs to a team in which you are an `owner`. To delete a calendar:

1. Navigate to your team dashboard.
3. Press the three dots button on a calendar record to open the context menu:<br/><img src="../../../img/manage/calendar/card-options.webp" alt="the edit calendar button" width="400px" style="box-shadow: 0px 0px 5px #ccc;
    -moz-box-shadow: 0px 0px 5px #ccc;
    -webkit-box-shadow: 0px 0px 5px #ccc;
    -khtml-box-shadow: 0px 0px 5px #ccc;"/>
4. Press the `delete` button.
5. You will be prompted to confirm the action:<br/><img src="../../../img/manage/calendar/card-delete-confirmation.webp" alt="the edit calendar button" width="400px" style="box-shadow: 0px 0px 5px #ccc;
    -moz-box-shadow: 0px 0px 5px #ccc;
    -webkit-box-shadow: 0px 0px 5px #ccc;
    -khtml-box-shadow: 0px 0px 5px #ccc;"/>
6. If you press `Yes`, the calendar will be deleted. Note that you won't be able to recover it later.
