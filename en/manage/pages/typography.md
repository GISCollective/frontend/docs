---
title: Typography
layout: default
parent: Pages
grand_parent: Managing your data
---

# Typography

To convey a clear and cohesive message, it's essential to maintain a consistent visual style across your pages. That's why we enable you to configure a global appearance for headings and body text, with options to further customize as needed. Additionally, we provide a curated selection of fonts available for use, all free of licensing requirements, from [https://fontsource.org/](https://fontsource.org/). If your preferred font isn’t listed, please reach out, and we’ll be happy to add it.

To edit your space fonts, you need to:
  1. Navigate to the edit space page. [More info here.](../../spaces/#editing-a-space)
  2. Scroll to the **font styles** section and press the **pencil** button to edit it:<br/><img src="../../../../img/manage/spaces/font_styles_edit.webp" alt="font edit view" width="90%" />
  3. Now you can go to each heading level and set the custom type face, weight and size:
  <video style="width: 100%; max-width: 540px;" preload loop autoplay>
    <source src="../../../../img/manage/spaces/fonts.mp4" />
  </video>
  5. Press save to generate the new style.

{: .info }

> The font styles are like a custom blend just for your website, crafted behind the scenes. If the changes don’t show up immediately, give it a moment—they’re on the way! Think of it like brewing a great cup of coffee; sometimes a little patience makes it just right!

## Using the fonts

To update your fonts, you need to:
  1. go to the edit mode for your page. [More details here](../#edit-the-page-content)
  2. select the component that you want to change (a title or a paragraph)
  3. update the text properties
  4. save

Here is an example for how you edit the font properties:

<video style="width: 100%; max-width: 540px;" preload loop autoplay>
  <source src="../../../../img/manage/pages/heading_edit.mp4" />
</video>

