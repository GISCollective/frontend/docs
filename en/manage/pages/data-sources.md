---
title: Data sources
layout: default
parent: Pages
grand_parent: Managing your data
---

# Data Sources

Along with basic content like text and images, our platform lets you showcase data you've added through the dashboard, such as maps, features, and surveys. We’ve set up the pages so you can display any of your stored data right on our platform. To make this possible, we introduced "data sources"—a simple way to connect a component to your data.

## How it works

Each component that supports external data has a set of data source properties, represented by a data icon: <img src="../../../../img/manage/pages/datasource-icon.webp" alt="data source icon" />. The properties you see will vary depending on whether the component displays one record or multiple records. When you select the component, the corresponding set of properties will be displayed.

### One record

When selecting a single record, the key option to consider is **use the matching record**. There are two settings:

  - **Yes**: This option works only on [pages with a path variable](../#editing-a-page) in the URL path. It selects the record based on that variable. For instance, if your page URL contains a *:feature-id* variable, the component will display the map feature that matches the ID from the browser URL. If the page lacks a variable, no record will be shown, and if no record matches the ID, a 404 error page will appear.

  - **No**: This option lets you manually choose other properties to help select the correct record.

The **type** option allows you to select the type of record you want to use. For example, if you want to display a map cover, choose the **map** option.

For certain types, like maps and surveys, you have the option to **use the global record**.

  - If you select **yes**, the record chosen on the [space edit page](../../spaces/#editing-a-space) will be used.
  - If you select **no**, you'll need to search for the record by name.

If the type doesn’t support global records, you can only select it by searching for its **name**.

### Multiple records
For components like **card list**, instead of selecting a single record, you need to define a query. After choosing the record **type**, you can select one of the following query types:

  - **All**: Selects all records of the chosen type that belong to your team. This is ideal for pages where visitors can browse all records, such as maps or surveys.
  - **Items**: Lets you select specific records by name, useful for showcasing a set list of items.
  - **Groups**: Displays records based on the page's state category, which is controlled by the **categories** component. This is helpful when you want to feature records grouped by custom categories rather than using icons alone.
  - **Parameters**: Allows you to define a list of property values from various sources:
      - **Ignored**: The query will not use this property.
      - **Default**: Uses the record defined in the **default models** on the space edit page. For example, a **map** property will use the default map, and a **calendar** property will use the default calendar.
      - **State**: Uses a dynamic state value set by special components like filters or categories.
      - **Fixed**: Lets you enter a custom query value.

For example, if you want to display the latest 4 articles, you would set:

  - **limit** to fixed **4**
  - **sortBy** to fixed **releaseDate**
  - **sortOrder** to fixed **asc**
