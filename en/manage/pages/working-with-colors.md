---
title: Working with colors
layout: default
parent: Pages
grand_parent: Managing your data
---

# Working with colors

We understand that after website content, design plays a crucial role in your site's success. To simplify your design process, we've added a user-friendly color palette feature, allowing you to easily manage and apply custom colors across your pages—no need to remember or reapply individual colors.

To edit your space color palette, you need to:
  1. Navigate to the edit space page. [More info here.](../spaces/#editing-a-space)
  2. Scroll to the **color palette** section and press the **pencil** button to edit it:<br/><img src="../../../../img/manage/spaces/color_palette_edit.webp" alt="color palette edit view" width="90%" />
  3. Click on a **color box** to open the color picker dialog:<br/><img src="../../../../img/manage/spaces/color_palette_picker.webp" alt="color palette edit view" width="90%" />
  4. Depending on your OS and version, the color picker might behave a little differently, but you can now choose a new color value.
  5. Press save to generate the new color palette.

We use the Bootstrap color palette system, so any colors you set will automatically become your theme colors. This means when you log in from your website to the dashboard, your custom colors will be applied. Plus, we generate lighter and darker shades, giving you more variety to work with on your site.

To learn more about the Bootstrap colors, [check this page](https://getbootstrap.com/docs/5.3/customize/color/#theme-colors).

{: .info }

> The color palette is like a special mix just for your website, and it’s being put together behind the scenes. If you don’t see the changes right away, just hang tight for a bit! Just remember, good things take time—like a chef perfecting a new recipe!

## Using the color palette

The color palette is available for every element that has a color, event to the text. You can change the colors in two ways:

 1. using the color picker component, for example the container background color:<br/><img src="../../../../img/manage/spaces/color_picker.webp" alt="color picker" width="90%" />

 2. using the style selector, for example a button style:<br/><img src="../../../../img/manage/spaces/button_style.webp" alt="style selector" width="90%" />

