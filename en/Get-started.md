---
title: Get started with mapping
layout: default
nav_order: 4
---


# Get started with mapping
{: .no_toc }
{: .fs-9 }

After creating an account and team, you are ready to set up your map! Here is an overview and a few tips to help you get started with confidence.
{: .fs-5 .fw-500 }
---

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

---
## Create Your First Map

Once a team is in place, it’s time to create a map. The map will contain _features_ that comprise the project data: points (sites), lines (routes) and polygons (areas). Make sure you are logged in and start by pressing on __+ > Map__:

<img src="/img/plus_map.webp" alt="plus-menu" width="400" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

When setting up a map, the first step is to name it, provide a description, and associate it to your team. Then you will get to the map’s Edit page, where a wide array of configurations are available.  It’s not required to use all of them, here are the most important ones:

### The Cover Photo

This image will be used as a map card when a list of maps is displayed. Upload a square photo, or one that will look nice when cropped to a square.

### Icon Sets

Each feature - whether it be a point, line, or polygon - is visible on the map with its primary icon. The available icons come from the map, so choose an existing icon set for the map, or create one if you want to use your own icons.

Initially, your map will have the default Green Map icons sets selected. You can click on the __pen__ button, then __enable__ to remove some icon sets or add your own.

<img src="/img/icon_sets.webp" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Once you selected the desired set or sets, click on Save.
In case you want to create your own icon set, find more info about that in our [Icon Design Guide](en/Icon-design-guide).

### The Mapping Area

The mapping area defines the geographic area of your map. It sets the centerpoint and scale of the map’s initial view. By defining a mapping area you are creating the default map view for your map. Ideally you want it zoomed in at an appropriate level to show several sites, so your map’s users are encouraged to explore further.

Setting the mapping area does not draw a border on the map, but centers that area, regardless if viewed on the desktop or a mobile device. It is also used to show which maps are available when people propose a site.

To set a mapping area:
1. First click on the pen button in the __mapping area__ section
2. Zoom in on the map, and move around to find the right location. You can move on the map by pressing on the mouse button and dragging while the mouse button is pressed.
3. Once you are over the right map area, position the blue dot where you want to start drawing a rectangle and click there(press and release the mouse button).
4. Now drag to make a rectangle of the right shape and size, then click again to confirm.

<img src="/img/mapping_area.webp" alt="mapping-area" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

If you would like to redraw the area, simply start again from step 1.

Before you save, if you'd like the mapping area to be a more precise area, press on __Custom Geometry__, then click in the rectangle to select it. You can now place the blue dot anywhere on the rectangle border, and press and drag to adjust the border shape as you wish. Then Save.

<img src="/img/custom_geometry.webp" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

### The “is public” Toggle

As long as it is private, only team members will have access to the map and its data.
You will want to make the map public when it's ready, or if your project includes public participation.

<img src="/img/map_public_toggle.webp" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

At this point, getting the word out about the map is important to plan. Some projects celebrate with a launch event, post on social media, and [embed their map on their own website](/en/Embed-map)!

## Contribute to the Map

Once you have the map, team members and the public at large can start contributing features and making use of the data. The Green Map platform offers many options for engaging people in the mapmaking process:


- By setting up one or more [__Surveys__](/en/Set-up-surveys) for your map, you can effectively crowdsource data gathering, even onsite, while you are in the field. The Open Green Map mobile app is perfect for gathering data even if an internet connection is not available on site.
- Using the import functionality you can add gpx, geojson, json and CSV data already collected.
- As the mapmaker, you can also [add data to your map](/en/Add-map-data) from __+ > Feature__ in the menu. This may be the quickest way to add individual features as a mapmaker, because after filling in the basic information (name, map and location) you go straight to the edit page of the feature, where you can also publish it.
