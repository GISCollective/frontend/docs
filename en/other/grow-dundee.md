---
title: Grow Dundee Members Guide
layout: default
nav_exclude: true
---

# Grow Dundee Members Guide
{: .fs-9 }
{: .no_toc }

---

[https://maxwell.greenmap.org](https://maxwell.greenmap.org).

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

## How to register

Grow Dundee is built with the Green Map platform, powered by GISCollective. You can log in with your Green Map account to manage your Garden information. If you don't have a Green Map account yet, go to Green Map's registration page to create your account: [https://new.opengreenmap.org/login/register](https://new.opengreenmap.org/login/register).

Once you activate your account, contact us and provide the email you used to register, so that we can add you to the team. As a team member, you can log in the Grow Dundee website. There, you can edit your garden details and add and edit volunteering opportunities and events.

## Events

### Add a new event

Most pages have a Submit section at the bottom, where you can submit an event. For example, scroll to the bottom of the Gardens page to find the _Submit a garden event_ option.

<img src="/en/other/grow-dundee/event-card.webp" alt="the event card" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

To create a new event, follow these steps:

1. Press the _Submit a garden event_ button, which will take you to the new event page.
2. Start by filling in the _Name_ of the event.
3. Fill in the event description in the _About_ field.
4. You can select one or more icons for the event. Make the selection by pressing the _+ select icons_ button:<br/>
  <img src="/img/manage/events/select-icons.webp" alt="the select icons button" width="150" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

5. Add one or more dates for your event by pressing the _+ event_ button:<br/>
  <img src="/img/manage/events/plus-event.webp" alt="the add event button" width="150" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

6. Select a cover image by pressing on the gray cover placeholder. A square picture will look best:<br/>
  <img src="/img/manage/events/plus-cover.webp" alt="the add cover button" width="150" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

7. Set a location for your event. Start typing your garden name in the _Location_ field, and you will receive suggestions with locations on the map. Alternatively, you can just type a location, e.g., an address. Note that in the latter case, the event won’t be associated with a map.
8. Press the _Add_ button to save the event. Then you will automatically get to the Edit page of the event.
9. On the _Edit_ page you can now review the event details, tweak any info if needed, and make the event public, so that it is visible to everyone. To make it public, set its visibility to _public_ in the panel on the right.

### Edit an event

To edit an event, navigate to the event's page, and press the downward arrow button located in the top right corner. There you'll find the _Edit_ option, which takes you to the event Edit page.

[Click here to learn more about how to use the edit event page](/en/manage/events/#editing-an-event)

### Delete an event

As a member you can not delete an event directly. If you want to remove an event, you can:

1. Make the event private, so that it won't be visible anymore. To do this you can navigate to the event Edit page, then set it to _private_ in the panel on the right.
2. Contact us to delete the event.

## Volunteering opportunities

### Add a volunteering opportunity

Most pages have a Submit section at the bottom, where you can submit a volunteering opportunity. For example, scroll to the bottom of the Volunteer page to find the _Submit a volunteer opportunity_ option.

  <img src="/en/other/grow-dundee/volunteering-opportunity-card.webp" alt="the volunteering opportunity card" width="200" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>

To create a new volunteering opportunity, follow these steps:

1. Press the _Submit a volunteer opportunity_ button, which will take you to the new volunteering opportunity page.
2. Start by filling in the _Name_ of the volunteering opportunity.
3. Fill in the description in the _About_ field.
4. You can select one or more icons. Make the selection by pressing the _+ select icons_ button:<br/>
  <img src="/img/manage/events/select-icons.webp" alt="the select icons button" width="200" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>
5. Set the schedule:<br/>
  <img src="/img/manage/events/schedule.webp" alt="the select icons button" width="500" style= " box-shadow: 0px 0px 5px #ccc;
        -moz-box-shadow: 0px 0px 5px #ccc;
        -webkit-box-shadow: 0px 0px 5px #ccc;
        -khtml-box-shadow: 0px 0px 5px #ccc;"/>
6. Optionally, select a cover image by pressing on the gray cover placeholder.
7. Set a location for the volunteering opportunity. If the location is your garden, start typing the garden name, and you will get a suggestion you can select. Otherwise type the address and/or other details for people to know to find the venue.
8. Press the _Add_ button to create the volunteering opportunity. Then you will automatically get to its Edit page.
9. On the _Edit_ page you can now review the details, tweak any info if needed, and make the volunteering opportunity public, so that it is visible to everyone. To make it public, set its visibility to _public_ in the panel on the right.

### Remove a volunteering opportunity

As a member you can not delete a volunteering opportunity directly. If you want to remove a volunteering opportunity, you can:

1. Make the volunteering opportunity private, so that it won't be visible anymore. To do this you can navigate to the volunteering opportunity Edit page, then set it to _private_ in the panel on the right.
2. Contact us to delete the volunteering opportunity.

## Gardens

### How to edit your garden data

1. Find your garden on the map or in the [gardens list](https://maxwell.greenmap.org/gardens).
2. Click on it to open the garden page.
3. In the top right-hand corner you should see a dropdown which contains the Edit option. In case that is not visible, please contact us. Pressing on Edit will take you to the Edit page, where you can edit the garden details.

The `Edit` page has two sections:
  - the main edit section, where you edit the content of your garden:
  - the sidebar on the right, which allows you to edit the metadata of the garden

To edit a field from one section, you generally need to press the edit button first, then make the updates, and press save. In the main section the edit button is represented by a pencil button <img src="/img/manage/pencil.webp" alt="the pencil button" style= "height:1.5em; box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>, and on the sidebar by an edit text <img src="/img/manage/edit.webp" alt="the edit button" style= "height:1.5em; box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>. The only exception is for the main article with the name and description (which is always editable and saved when the save button is pressed), and the icons, which are saved automatically when updated.

To help you to better understand what each field does, here is a list with explanations:

  - **The main article** is the article that will be shown on the website, containing the garden name and description.
  - **icons** are the icons associated to the garden. You can remove icons by pressing on the x, and add more icons by pressing on _+ select icons_. You can also reorder the icons by dragging them to the left or right. Please do not change the primary icon(Garden).
  - **maps** sets the maps the the garden is displayed on. Please do not change the map of the garden.
  - **geometry** is the location of the garden on the map.
  - **Photos** are the photos in the garden's photo gallery.
  - **Sounds** are the playable sounds associated with the garden. This is optional and we expect that few or no gardens will want to upload sound files.
  - **attributes** are the details of the garden. Please feel free to edit existing detail fields, but do not add new attributes. If you'd like a new field added to the garden please contact us.


## Contact Us

Grow Dundee is facilitated by **The MAXwell Community Center** Located in St. Salvadors's Scottish Episcopal Church:

__*Carnegie St, Dundee DD3 7EW*__

email: [growdundee@gmail.com](mailto:growdundee@gmail.com)

01382 802628
