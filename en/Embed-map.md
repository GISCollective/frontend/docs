---
title: Add your map to your website
layout: default
nav_order: 7
---


# Add your map to your website
{: .fs-9 }

Presenting an interactive map on your website is easy with the option to embed maps. This way your website visitors can explore the map without changing context and the map's latest updates are always reflected.
{: .fs-5 .fw-500 }
---

At the bottom of the Map Edit page, find the embed code in the __Embedded map__ section. It should look similar to this:

<img src="/img/embed-code.webp" alt="campaign-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Copy and paste the whole text on the website you want to embed the map to.
You may need to review how embeds work on your specific CMS or website tool but generally pasting the full HTML text is enough.

Save and see if that worked (you might see a white box, give it a few seconds to load). You can add text that describes the map and links to the full sized, fully featured map on OGM2.

Also check how the search bar looks: you may want to edit the height of the embedded map, to make sure it is big enough when people interact with the search functionality.

__Note:__ On Squarespace use #3 "Add an Embed Block" section: Alternatively, click the </> icon in the address field to add or edit the embed code manually. So click that "</>" icon and paste the whole code from the "Embedded map" section. Save and the map should now be embedded.

## Customizing the embedded map
Our embedding code has some options to alter the default behaviour. Here is an explanation of the options of the embedded map:

  - **show the map info box** - This feature enables you to toggle the visibility of the map's information box.
  - **enable the show more button** - By toggling this option, you can choose whether to display or hide the 'View More' button when a feature is selected.
  - **filter with more than one icon** - This option modifies the default behavior, allowing you to select more than one icon for filtering. When selected, you can filter features that match all of your chosen icons.
  - **search panel expands all available icons** - This setting expands the search panel to display all available icons directly on the map. This eliminates the need to first select an icon set before choosing icons.

## Changing the default view
Unfortunately there is no visual way for now to customize the default view of the embeded map. But with a basic understanding of an web address(URL), you can set the initial view area of your iframe.

### Step 1: Define the View Box
Navigate to the Green Map world view and adjust the map area to the desired visibility for your embedded map. As you make changes, observe that the URL will update to include a "viewbox" query parameter. For instance, zooming over Berlin might result in a URL similar to this:

```
https://new.opengreenmap.org/browse/maps/_/map-view?viewbox=12.56612438669925%2C52.26823195315208%2C13.931660325237011%2C52.74649952333516
```

From this url you need to copy the **viewbox=12.56612438669925%2C52.26823195315208%2C13.931660325237011%2C52.74649952333516** part.

### Step 2: update the url for the iframe

The embedded HTML code includes an HTML `<iframe>` tag with a basic URL. To customize the default view of this map, incorporate the variable you retrieved in the initial step.

For example, if your embed code looks like this:

```
    <iframe src="https://new.opengreenmap.org/browse/maps/5ca89e44ef1f7e010007f5e3/map-view?embed=true&mapInfo=true&showMore=true&filterManyIcons=false&allIcons=false"
    allowFullScreen="true" allow="fullscreen; geolocation;" title="Aktionen & Events - Open Greenmap Berlin" frameborder="0" width="100%" height="500"></iframe>

```

It should look like this:

```
 <iframe src="https://new.opengreenmap.org/browse/maps/5ca89e44ef1f7e010007f5e3/map-view?embed=true&mapInfo=true&showMore=true&filterManyIcons=false&allIcons=false&viewbox=12.56612438669925%2C52.26823195315208%2C13.931660325237011%2C52.74649952333516"
    allowFullScreen="true" allow="fullscreen; geolocation;" title="Aktionen & Events - Open Greenmap Berlin" frameborder="0" width="100%" height="500"></iframe>
```

Essentially, you need to append the **&** character followed by the viewbox query parameter to the end of the source URL. Here's the modified part highlighted in bold:

new.opengreenmap.org/browse/maps/5ca89e44ef1f7e010007f5e3/map-view?embed=true&mapInfo=true&showMore=true&filterManyIcons=false&allIcons=false<b class="text-success">&viewbox=12.56612438669925%2C52.26823195315208%2C13.931660325237011%2C52.74649952333516<b>