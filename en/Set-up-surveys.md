---
title: Set up a Survey
layout: default
nav_order: 2
parent: Surveys
---


# Set up a Survey
{: .fs-9 }

Here is how you can create a Survey in 6 steps.
{: .fs-5 .fw-500 }
---

## Step 1

From the Menu, access the Survey setup page from __+ -> Survey__ and fill in some basic information: set the name, and write a concise article that explains the motivation of the project, why people should suggest a place for the Green Map, and how their perspective and contribution of knowledge is valued. This article will serve as the description of the survey you are creating. Lastly, select the team that owns the survey.

<img src="/img/plus_survey.webp" alt="campaign-form" width="200" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

## Step 2

Adding an attractive image as the cover photo gives the Survey a more personal touch. Portrait (not landscape) image format is preferred.

## Step 3

Select and save the map to be associated with the Survey.

<img src="/img/survey_map.webp" alt="campaign-form" width="500" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

## Step 4

Next it’s time to choose the icons. The default icons are automatically added to each suggested site.


For example, if you want to collect data on old trees, you can choose the Special Tree icon. Select it as default, so that all the sites will have this icon.

<img src="/img/default_icon.webp" alt="campaign-form" width="500" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

You can also choose some optional icons that will appear in the order selected by the submitter.
Contributors can choose whether to add any of them or none. For the tree data scenario, these optional icons could be ones that could tell you more about the site, like what context or habitat the tree is located in. You can add as many icons as you like, but consider your participants and their capacity as you set up the icon options they can choose from.
The optional icons are added from the __questions__ section of the Survey. If you want you can also set a custom question that will guide submitters to choose optional icons.

<img src="/img/survey_add_optional_icons.webp" alt="campaign-form" width="500" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

## Step 5

There are other questions you can set up or tweak for your survey.

For example, you can choose how you want contributors to be able to enter the location when they fill in the survey. Use the checkboxes in the _question for the feature location_ section to restrict or add options.

Another example is the name field: if you don’t require a name field for the site contributions, each entry will automatically get an id number. You can set a prefix for that id in the panel on the right, at the top of the edit page. This way browsing through data on the map will be easier. For the old trees Survey example, since it is not required that participants give a name to the site, so you could add the prefix “Old tree”. This way all entries will be of the form “Old tree 602f9d163165ac0100aaf000”, “Old tree 323f9d163165ac0100aaf111”, so it’s clear users are looking at a list of old trees entries.
If instead you would like to collect a Name for the site, you can set a custom question for the name.

You also have a question for the description field. Frame questions to collect the type of information you want about the site.

Questions for media upload are available, if you'd like to collect pictures or sound files as part of the survey.

There is also a _contributor questions_ category, where you can set up questions related to the personal information of the contributors. To ensure contributors' privacy, the answers to these will be kept private in your survey answers list, and will not be published on the map.

You also have the option to restrict participation to registered users. Otherwise, anonymous contributors, without being logged in, can also submit answers to Surveys. The toggle to restrict participation is in the panel on the right, at the top of the edit page.

## Step 6

And finally, make your Survey public, so that people can start participating!

You do that by enabling the _is public_ toggle in the side bar on the right.

Once the Survey is public, it will be accessible from Surveys in the menu, and ready for contributions.
You can make a short **web address** (e.g. on bit.ly) or QR code for the Survey. Use the cover image for your social media and other outreach. Let the local news and radio know, stage an event, and otherwise, get the word out!
