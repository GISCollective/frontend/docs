---
title: Define the team
layout: default
nav_order: 3
---


# Define the team
{: .fs-9 }

After creating an account, the first step is to set up your team.
{: .fs-5 .fw-500 }
---

Start by setting up a new team for your project, and designating roles. On this platform, the team owns the _maps_, their _features(points, lines and polygons)_, as well as any specific _icons_ and _base maps_ you may use for your project. Basically all of your project’s data is managed by the team. The platform enables you to define who can view, download, share, or edit the data.

As Green Mapmaker, you can create teams for different maps and assign different roles for each participant. You can create your first team by going to your dashboard from the menu under your user name.

<img src="/img/user_menu.webp" alt="user-menu" width="400" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Then press __+ > Team__ in main bar.

<img src="/img/plus_team_menu.webp" alt="plus-menu" width="400" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

When creating a team, you are by default an Owner. If you plan to work alone, you can make a team with yourself as the only member. If however you have a group, classroom or volunteers collaborating on the project, the platform offers a set of roles that you can set to define different access levels. Here is an overview of the 4 roles currently available:

- __Owners__ have full access to all data related to your team. They can create, update or delete any maps, features, icon sets, even the team itself.
- __Leaders__ can view and edit the team’s maps and features. This means they can review and update or delete any data owned by the team, even if it is private.
- __Members__ can add and edit their own map features. They can view all private data on this map, but can only change what they own.
- __Guests__ can view the project data, but cannot modify it in any way. This role can be useful for providing early access to your private data sets.

For more details on the roles, you can check [our access rights documentation](/en/Access-rights){:target="_blank"}.

The __New team__ page will ask you for a Team name and a few words about your team, which can include your location, mission, themes, etc. You can add more details about your team, including images, later, on the edit page.

Once you create the team, you can invite your colleagues to join. Adding users to the team is done in the __Members__ section by searching them by email and selecting the appropriate role. Note that each person needs to create their own account on the platform for you to find them on the list and be able to invite them to your team:

<img src="/img/team-members.webp" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>

Many Green Mapmakers create different kinds of maps with different partners and it’s possible to create more than one team. It's important to note that every map is owned by one team.

For public contributions, you do not need to add participants to the team. If your map is public, people can propose sites or submit suggestions via your Surveys as logged in users, or as anonymous users.
