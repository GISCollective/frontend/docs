---
title: Installation
layout: default
nav_order: 2
parent: Develop
---

# Installing GISCollective

GISCollective is an open source platform that aims to empower collaboration and participation on maps. Part of living up to this goal is to make the platform accessible to as many people and organizations as possible, so we offer the possibility to install it on your own infrastructure.

The platform is scalable and designed to be used in a cloud environment. At the same time, not all organizations need to manage big data environments. Because of that we offer two installation options:

  - The first method is using our docker container bundle which contains all services that you need to run the platform.
  - The second one is using a helm chart that allows you to scale the app based on your needs.

For both installation methods you need to have basic knowledge about using a terminal.


## Why use your own infrastructure?

There are multiple reasons that could make using your own infrastructure preferable to using a public instance for GISCollective like [app.giscollective.com](app.giscollective.com). Here are a few common situations:

  - You may have strict security restrictions that make using a public instance not desirable, since the data is not hosted by you directly.
  - You want to host GISCollective in a data center that is closer to you.
  - You want to be 100% in control of your data and hardware.
  - Or maybe you just want to try out the platform and customize it.

## How to install GISCollective using Docker

The easiest and the fastest way to run GISCollective is using our official bundle that can be found on Docker Hub: [https://hub.docker.com/r/giscollective/giscollective](https://hub.docker.com/r/giscollective/giscollective)

Once you installed Docker or any other compatible container engine like Podman, you can run this command to start a stateless instance:

```
docker run -p 8080:8080 giscollective/giscollective
```

Once the container finished the initialization you can access it using the browser at [http://127.0.0.1:8080/](http://127.0.0.1:8080/) with the user email `admin@example.com` and password `admin`.

If you want to keep the data, logs or have a custom configuration, you need to mount the volumes exposed by the container. You can also add a name, to identify the container later, run it in a detached mode and restart it in case of failure. So the final command should look like this:

```
> docker run --detach -p 8080:8080 \
    --name giscollective \
    --restart always \
    --volume $GISCOLLECTIVE_HOME/data:/app/data \
    --volume $GISCOLLECTIVE_HOME/logs:/app/logs \
    --volume $GISCOLLECTIVE_HOME/config:/app/config \
    giscollective/giscollective
```

Or if you are in a SELinux environment like Fedora or CentOS:

```
> docker run --detach -p 8080:8080 \
    --name giscollective \
    --restart always \
    --volume $GISCOLLECTIVE_HOME/data:/app/data:Z \
    --volume $GISCOLLECTIVE_HOME/logs:/app/logs:Z \
    --volume $GISCOLLECTIVE_HOME/config:/app/config:Z \
    giscollective/giscollective
```

To get the latest logs of this container you can check the logs folder, or you can run:

```
> docker logs -f giscollective
```

Regular backups can be set for the `data` folder, and if it’s needed you can add some custom configurations to the files generated in the `config` folder. You can also expose this container to the internet by setting a reverse proxy, but we won’t cover that topic here, since it can vary from system to system.
How to install GISCollective using Helm

This method can be used for environments that will have a lot of data or users, or need features like auto scaling or high availability. In this case you may want to run the platform on a [kubernetes](https://kubernetes.io/) cluster. Our helm chart will do the setup for all pods and cron jobs that are needed to run GISCollective, and it will define the services that will publish the platform to the internet, including SSL certificates if you already have a [cert-manager](https://cert-manager.io/) deployment. Before you start the app, here are the steps you need to do:

1. The first one is to make sure that your cluster can download docker images from gitlab.com:

```
> docker login registry.gitlab.com
> cp /root/.docker/config.json ~/.docker
> kubectl create secret generic regcred --from-file=.dockerconfigjson=~/.docker/config.json --type=kubernetes.io/dockerconfigjson
> kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "regcred"}]}'
```

2. The second step is to define a service called `db-service` for your MongoDB database. We can not cover in detail the setup for the `db-service` since you might have different setups. If you are not sure about how to setup this service, check [the kubernetes service documentation](https://kubernetes.io/docs/concepts/services-networking/service/) and [the mongo kubernetes setup documentation](https://www.mongodb.com/kubernetes).

3. The third thing you need to do is to clone our helm repo and build the chart:


```
> git clone https://gitlab.com/GISCollective/kubernetes-setup.git
> cd kubernetes-setup
> helm package giscollective
```

Successfully packaged chart and saved it to: kubernetes-setup/giscollective-x.x.x.tgz

4. And now you can install the platform on your cluster using the helm install command:

```
> helm install --namespace namespace-name deployment-name ./giscollective-x.x.x.tgz --set serviceName="GISCollective",clientName=clientName,domainName="example.com"
```

All variables except the deployment-name are optional, and they come with default values that will work if you use only one deployment. If you want to tweak them, here is an explanation for the variables used here:

  - `namespace-name` is optional but recommended if you have a complex cluster setup where you host more than one deployment of GISCollective. You can read more [about namespaces here](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/). We usually prefer to use the `clientName` value as the namespace.
  - `deployment-name` this is the name of the helm deployment. You will use the same name to upgrade the deployment to a newer version.
  - `clientName` is used to set up various internal variables like the internal queues and the `MongoDb` database names.
  - `example.com`, the domain name variable, will be used to setup some email templates.

Let’s assume that you want to deploy the the platform for `GreenMap` at `greenmap.com`. The install command should look like this:

```
> helm install --namespace green-map green-map ./giscollective-x.x.x.tgz --set serviceName="Green Map",clientName=green-map,domainName="greenmap.com"
```

In a few moments the app will be deployed and available on your cluster. You can check the status with helm status:

```
> helm --namespace green-map status green-map
NAME: green-map
LAST DEPLOYED: Wed Jun 30 19:22:55 2021
NAMESPACE: green-map
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

Once the helm chart is deployed, you can go to your domain and log in with the user email `admin@example.com` and password `admin`.

Attention! Delete this admin account once you set up your own account, since it can be a security vulnerability.
{: .label .label-red }
{: .fs-3 }

Updating the deployment can be done using [helm upgrade](https://helm.sh/docs/helm/helm_upgrade/):

```
> helm upgrade --namespace green-map green-map ./giscollective-x.x.x.tgz
```

## In conclusion

Installing GISCollective on your infrastructure is straightforward, with two setup options available to choose from. For the easier workflow use the Docker installation, and for a scalable setup with high availability use the Helm installation.

If you get stuck during this process or you want to help us improve our installation workflow, you can always contact us on email at [hello@giscollective.com](mailto:hello@giscollective.com) or via our [Service Desk](https://gitlab.com/GISCollective/help-desk/-/issues).
