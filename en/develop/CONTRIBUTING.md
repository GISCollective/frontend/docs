---
title: Contributing
layout: default
nav_order: 3
parent: Develop
---

# Contributing to GISCollective

Thank you for your interest in contributing to GISCollective!
Please see the project's [Code of Conduct](/en/developers/CODE_OF_CONDUCT/) before proceeding. We expect that all contributors abide by our Code of Conduct.

## Asking questions, bug reports and feature requests

The main place where we discuss about all of these topics is our [Service Desk](https://gitlab.com/GISCollective/help-desk/-/issues). There you can find all issues raised over time by our community. We try to focus to respond to all issues as soon as possible. Make sure you do a quick search for your question before you submit a new one.

If you are not familiar with GitLab or you don't want to create an account, you can also contact us by email at [hello@giscollective.com](mailto:hello@giscollective.com).


## Getting Familiar with the code

Each project has a `readme.md` file. There is just one such file for each repository and it contains all you need to know to work with the code from that repository.


## Contributing Code

The platform's code spreads over multiple repositories. Public contributions for each repository should be done using [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/). Please make sure that your merge request follows our [merge request guidelines](#merge-request-guidelines) below before submitting it.


## Contributor License Agreement

Your contributions will be under [GNU Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.html) for all the repositories that are part of the GISCollective platform and [MIT License](https://opensource.org/licenses/MIT) for all the libraries that we are maintaining. You can check the license file in each of our repositories.


## Merge request guidelines

Open a merge request even if your contribution is not ready yet! It can be discussed and improved collaboratively! You may prefix the title of your pull-request with `WIP: ` to make it clear that it is not yet ready for merging. If there is an issue in our [Service Desk](https://gitlab.com/GISCollective/help-desk/-/issues), add a link to it in the merge request so we can track it. If you are not sure if the contribution is suitable for our platform, add an issue to the [Service Desk](https://gitlab.com/GISCollective/help-desk/-/issues) explaining what you want to contribute. This ensures that your merge request won't go unnoticed, and that you are not contributing something that is not suitable for the project.

Your merge request must:

 * Follow GISCollective's coding style.

 * Add unit tests for the code that you write. We favour [TDD](https://en.wikipedia.org/wiki/Test-driven_development) and [Software Craftsmanship](https://manifesto.softwarecraftsmanship.org/) practices.

 * Address a single issue or add a single item of functionality.

 * Contain a clean history of small, incremental, logically separate commits,
   with no merge commits.

 * Use clear commit messages.

 * Be possible to merge automatically.
