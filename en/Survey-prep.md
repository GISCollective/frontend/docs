---
title: Preparation
layout: default
nav_order: 1
parent: Surveys
---


# Preparation
{: .fs-9 }

Before setting up your Survey, you should already have created your __Team__, selected __Icon sets__ and set up the __Map__ that will receive the Survey’s contributions.
{: .fs-5 .fw-500 }
---


## Team

If you don't already have a team on the platform, [here](../Create-team) is how to create one.
The map's team will receive notifications of sites submitted on Surveys associated with the map. Based on the roles, team members can edit and publish the submitted sites.

## Map

To create a Survey, you must have a map set up first to receive the data submitted through the Survey. In case you have skipped this, [here](../Get-started/#create-your-first-map) are the steps to create one.

All newly suggested sites are private. The map team will be notified via email when a Survey contribution is made, and the sites can be reviewed, possibly edited, and published.

You can also set suggested data as “pending”, in the side bar on the right of the map's edit page. This means that contributors will see the site's icon on the map right after submitting their answer, however the actual site data will not be visible until reviewed and published, so the icons on the map are not clickable for "pending" sites.

## Icons

Although Green Map has co-created an award winning set of icons used on all Green Map, we have also created a way for users to use their own custom icon set. One advantage of doing this is that it will allow you to add additional questions to your Survey. These can have special attributes as described below. This is a more advanced step, so feel free to simply use the existing icon sets.

The icons are an important aspect of the Surveys, because you can associate questions to an icon, and they will show up in the Survey form whenever that icon has been selected by the user. We call these icon attributes, and they offer flexibility in defining what type of answer you expect: text, numbers, predefined options, or boolean values(true or false).  So for example if you’d like to collect data on ancient trees, you may need certain attributes of a tree: you could define the tree diameter as an icon attribute, and the tree species as another icon attribute. In addition to seeing it on the map, this data can be exported from the map.

<img src="/img/icon-with-attributes.webp" alt="register-form" width="700" style= " box-shadow: 0px 0px 5px #ccc;
      -moz-box-shadow: 0px 0px 5px #ccc;
      -webkit-box-shadow: 0px 0px 5px #ccc;
      -khtml-box-shadow: 0px 0px 5px #ccc;"/>
