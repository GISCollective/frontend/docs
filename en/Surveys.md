---
title: Surveys
layout: default
nav_order: 6
has_children: true
---


# Surveys
{: .fs-9 }

If you have a mapping project based around public participation, setting up a Survey is a great option for making it straightforward for people to contribute.
{: .fs-5 .fw-500 }
---

With the Green Map platform we offer everyone an accessible and user-friendly mapping tool for promoting sustainable communities. Using Surveys, you get a form that is specific to your project – you can personalize it with the project description and a picture, choose the icons you want to use, and customize the questions you want to ask.

From the start, you tie the Survey to a specific map, so that responses to the Survey will be automatically linked to that map once submitted. You then review each contribution before publication. You can create more than one Survey per map. Visit the pages below to get started.
