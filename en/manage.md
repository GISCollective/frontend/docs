---
title: Managing your data
layout: default
nav_order: 9
has_children: true
---


# Managing your data
{: .fs-9 }

Our platform empowers you with the flexibility to oversee a multitude of data types, spanning from mapping data to surveys and events. This dedicated section is designed to provide you with comprehensive guidance on how to adeptly navigate and manage this diverse array of data, ensuring that you can make the most of the functionalities available to you.