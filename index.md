---
layout: default
title: Overview
nav_order: 1
description: "The Green Map Platform is a tool for charting nature, culture, social justice and sustainable living resources"
permalink: /
seo_tag:
    page_title: "Green Map Guides"
seo:
    name: "Green Map Guides"
---
<img src="/img/gm-logo.webp" alt="register-form" width="85" align="right"/>

# Mapping for sustainability
{: .fs-9 }

Welcome to the Green Map Platform, made for charting nature, culture, climate justice and sustainable living resources.
{: .fs-6 .fw-300 }
---

# Introduction

Designed for community mapping, this platform is ready for all kinds of mapmakers - from students to professionals - who want to draw attention to sustainability in their own city, town or community.

The Green Map Platform is versatile, allows embedding, import and export of data, and it is usable on any type of device. We also offer a free iOS app which you can download from the [AppStore](https://apps.apple.com/de/app/open-green-map/id1587762269?l=en#?platform=iphone){:target="_blank"}.

The Green Map Platform uses Green Map System’s globally designed iconography. You can add your own symbols, too, and use them alongside our 170 icons for nature, culture, social justice and green living. Each site, area or route can display multiple icons. We recommend setting up to 8 icons. Each of your sites can be included on more than one map. You can cover a simple theme map or make a comprehensive overview of sustainable, climate-smart and healthy resources. See the sets of Icons under Explore at [Icons](https://greenmap.org/explore/icons).

Much more about this nonprofit’s global network and impacts in 65 countries, including its shared engagement and mapping resources, and inspiring examples and outcomes is at [GreenMap.org](https://GreenMap.org){:target="_blank"}.
You can also find more details about the platform and why you should use it, on [this page](https://greenmap.org/why-green-map){:target="_blank"}.

The Green Map Platform is powered by the [GISCollective](https://giscollective.com/){:target="_blank"}’s open source mapping platform. Designed for and with the Green Map network, it is the second open Green Map platform, based on open source software.

## How to use this guide

From this guide, learn to become a Green Mapmaker, and how to set up your own mapping project using this platform.

- If you are a new mapmaker and don't yet have an account, [start here](/en/Create-account).

- Already have an account? Start by [defining your team](/en/Create-team).

- If your team is set up, skip to [Get started with mapping](/en/Get-started).

If you would like to contribute to the code or install the platform locally, go to [Develop](en/Develop).
{: .fw-500 .text-blue-300}
